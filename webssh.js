/* 拼接websocket连接地址 */
function get_connect_info() {
    /* 获取表单参数（ssh连接相关） */
    var auth = $("input[name='auth']:checked").val();
    var pwd = $.trim($('#password').val());
    var password = window.btoa(pwd);
    var ssh_key = null;

    if (auth === 'key') {
        var pkey = $('#pkey')[0].files[0];
        var csrf = $("[name='csrfmiddlewaretoken']").val();
        var formData = new FormData();

        formData.append('pkey', pkey);
        formData.append('csrfmiddlewaretoken', csrf);

        $.ajax({
            url: '/upload_ssh_key/',
            type: 'post',
            data: formData,
            async: false,
            processData: false,
            contentType: false,
            mimeType: 'multipart/form-data',
            success: function (data) {
                ssh_key = data;
            }
        });
    }
    // console.log(host, port, user)
    var connect_info1 = 'host=' + host + '&port=' + port + '&user=' + user + '&auth=' + auth;
    var connect_info2 = '&password=' + password + '&ssh_key=' + ssh_key;
    return connect_info1 + connect_info2
}

/* 获取终端大小 */
function get_term_size() {
    var init_width = 9;
    var init_height = 20;
    // var init_height = 18;

    var windows_width = $(window).width();
    var windows_height = $(window).height();

    return {
        cols: Math.floor(windows_width / init_width),
        // rows: Math.floor(windows_height / init_height),
        rows: Math.floor(windows_height / init_height) + 1,
    }
}

/* websocket逻辑 */
function websocket() {
    /* 声明全局变量 */
    host = $.trim($('#host').val());
    port = $.trim($('#port').val());
    user = $.trim($('#user').val());

    var cols = get_term_size().cols;
    var rows = get_term_size().rows;
    var connect_info = get_connect_info();

    var term = new Terminal(
            {
                cols: cols,//列数
                rows: rows,//行数
                useStyle: true,
                cursorBlink: true, //光标闪烁
                convertEol: true, //启用时，光标将设置为下一行的开头
                rendererType: "canvas", //渲染类型
            }
        ),
        protocol = (location.protocol === 'https:') ? 'wss://' : 'ws://',
        socketURL = protocol + location.hostname + ((location.port) ? (':' + location.port) : '') +
            '/webssh/?' + connect_info + '&width=' + cols + '&height=' + rows;

    // 创建WebSocket连接
    // console.log(socketURL)
    socket = new WebSocket(socketURL);

    // 创建好连接之后自动触发（ 服务端执行self.accept() )
    socket.onopen = function () {
        // 隐藏表单
        $('#form').addClass('hide');
        // 显示终端
        $('#django-webssh-terminal').removeClass('hide');
        term.open(document.getElementById('terminal'));
        // 设置body黑色背景
        document.body.style.background = 'black'
        // 输出进入终端提示信息
        term.write(`\x1b[31mDjango Web SHELL ~\x1b[m\r\n\r\n`)

        // 添加一个状态div
        tag = document.createElement("div");
        tag.id = 'status'
        tag.innerText = '[' + user + '@' + host + ':' + port + '] 终端连接中...';
        document.body.appendChild(tag);
    }

    // 当websocket接收到服务端发来的消息时，自动会触发这个函数。
    socket.onmessage = function (event) {
        // 解析接收数据
        var data = JSON.parse(event.data);
        var message = data.message;
        var status = data.status;
        if (status === 0) { // 返回消息正常，输出到终端页面
            term.write(message)
            tag.innerText = '[' + user + '@' + host + ':' + port + '] 终端已连接 ~';
        } else { // 连接异常，终端断开
            term.write("\n" + message)
            tag.innerText = '[' + user + '@' + host + ':' + port + '] 终端已断开 ~';
            /*
            alert(message)
            window.location.reload()
             */
        }
    }

    // 服务端主动断开连接时，这个方法也被触发。
    socket.onclose = function (event) {
        term.write("\n\n终端已断开 ~ 请刷新页面重试！")
        tag.innerText = '[' + user + '@' + host + ':' + port + '] 终端已断开 ~';
    }

    /*
    * status 为 0 时, 将用户输入的数据通过 websocket 传递给后台, data 为传递的数据, 忽略 cols 和 rows 参数
    * status 为 1 时, resize pty ssh 终端大小, cols 为每行显示的最大字数, rows 为每列显示的最大字数, 忽略 data 参数
    */
    const message = {'status': 0, 'data': null, 'cols': null, 'rows': null};

    // 通过 Terminal（xterm.js） 向服务器端发送数据
    term.on('data', function (data) {
        message['status'] = 0;
        message['data'] = data;
        socket.send(JSON.stringify(message))
    });

    // 监听浏览器窗口, 根据浏览器窗口大小修改终端大小
    $(window).resize(function () {
        var cols = get_term_size().cols;
        var rows = get_term_size().rows;
        message['status'] = 1;
        message['cols'] = cols;
        message['rows'] = rows;
        socket.send(JSON.stringify(message));
        term.resize(cols, rows)
    })
}