#!/bin/bash

# 输出信息
echo -e "\nLNMP INSTALL...!\n"

# 1、安装 nginx

# 输出信息
echo -e "\nnginx INSTALL...!\n"

yum install -y gcc gcc-c++ openssl-devel zlib-devel pcre-devel 
useradd -r -s /sbin/nologin www
tar -zxf nginx-1.14.0.tar.gz
cd nginx-1.14.0
./configure --prefix=/usr/local/nginx1.14 --with-http_dav_module --with-http_stub_status_module --with-http_addition_module --with-http_sub_module --with-http_flv_module --with-http_mp4_module --with-http_ssl_module --with-http_gzip_static_module --user=www --group=www && make -j$(nproc) && make install 
ln -sf /usr/local/nginx1.14/sbin/nginx /usr/local/sbin/
cd ..
# nginx启动脚本
cat> /etc/init.d/nginx <<"EOF"
#/etc/init.d/nginx 添加执行权
#!/bin/bash
# chkconfig: - 85 15
# description: nginx is a World Wide Web server. It is used to serve
nginx=/usr/local/nginx1.14/sbin/nginx
case $1 in
start)
netstat -anptu|grep nginx
if [ $? -eq 0 ]; then
echo "nginx server is already running"
else
echo "nginx server begin start...."
$nginx

fi
;;
stop)
$nginx -s stop
if [ $? -eq 0 ]; then
echo "nginx server is stop"
else
echo "nginx server stop fail,try again!"
fi
;;
status)
netstat -anptu|grep nginx
if [ $? -eq 0 ]; then
echo "nginx server is running"
else
echo "nginx server is stoped"
fi
;;
restart)
$nginx -s reload
if [ $? -eq 0 ]; then
echo "nginx server is begin restart"
else
echo "nginx server restart fail!"
fi
;;
*)
echo "please enter {start|restart|status|stop}"
;;
esac
EOF

chmod +x /etc/init.d/nginx
chkconfig --add nginx
chkconfig nginx on
/etc/init.d/nginx start

# 2、安装 mysql：使用脚本安装，root，123

# 输出信息
echo -e "\nmysql INSTALL...!\n"

tar -zxf mysql-5.7.22-linux-glibc2.12-x86_64.tar.gz
#将二进制包移动到/usr/local/mysql
mv mysql-5.7.22-linux-glibc2.12-x86_64 /usr/local/mysql
#创建data目录
mkdir /usr/local/mysql/data
#创建用户和组并赋予文件夹权限
groupadd -r mysql
useradd -r -g mysql -M -s /bin/false mysql
chown -R mysql:mysql /usr/local/mysql
#删除centos7中自带的mariadb-libs
rpm -e mariadb-libs --nodeps
#设置my.cnf配置文件
cat> /etc/my.cnf <<EOF
[client]
socket=/usr/local/mysql/mysql.sock
[mysqld]
basedir=/usr/local/mysql
datadir=/usr/local/mysql/data
pid-file=/usr/local/mysql/data/mysql.pid
socket=/usr/local/mysql/mysql.sock
log-error=/usr/local/mysql/data/mysql.err
EOF
#做服务的软链接
ln -sf /usr/local/mysql/bin/* /usr/local/bin/
#mysql初始化
mysqld --initialize --user=mysql --basedir=/usr/local/mysql/ --datadir=/usr/local/mysql/data
#服务自启动
mv /usr/local/mysql/support-files/mysql.server /etc/init.d/mysqld
chkconfig --add mysqld
chkconfig mysqld on
#启动mysql并修改密码为123
/etc/init.d/mysqld start
mysqlpwd=`grep password /usr/local/mysql/data/mysql.err |awk -F 'root@localhost: ' '{print $2}'`
mysql -uroot -p${mysqlpwd} -e 'alter user root@localhost identified by"123"' --connect-expired-password

# 3、安装 php

# 输出信息
echo -e "\nphp INSTALL...!\n"

# 	1)安装 libmcrypt
tar zxf libmcrypt-2.5.7.tar.gz
cd libmcrypt-2.5.7/
./configure && make && make install
cd ..

# 	2)安装 PHP
yum install -y libxml2-devel bzip2-devel freetype-devel libcurl-devel libjpeg-devel libpng-devel 
tar zxf php-5.6.27.tar.gz
cd php-5.6.27/
./configure --prefix=/usr/local/php5.6 --with-config-file-path=/etc --with-mysql=/usr/local/mysql --with-mysqli=/usr/local/mysql/bin/mysql_config  --with-mysql-sock=/usr/local/mysql/mysql.sock --with-gd --with-iconv --with-libxml-dir=/usr  --with-mhash --with-mcrypt --with-config-file-scan-dir=/etc/php.d --with-bz2 --with-zlib --with-freetype-dir --with-png-dir --with-jpeg-dir --enable-xml --enable-bcmath --enable-shmop --enable-sysvsem --enable-inline-optimization --enable-mbregex --enable-fpm --enable-mbstring --enable-ftp --enable-gd-native-ttf --with-openssl --enable-pcntl --enable-sockets --with-xmlrpc --enable-zip --enable-soap --without-pear --with-gettext --enable-session --with-mcrypt --with-curl && make -j$(nproc) && make install
cp php.ini-production /etc/php.ini
sed -i 's/;date.timezone =/date.timezone = PRC/' /etc/php.ini
sed -i 's/expose_php = On/expose_php = Off/' /etc/php.ini 
sed -i 's/short_open_tag = Off/short_open_tag = On/' /etc/php.ini
sed -i 's/post_max_size = 8M/post_max_size = 16M/' /etc/php.ini  
sed -i 's/max_execution_time = 30/max_execution_time = 300/' /etc/php.ini 
sed -i 's/max_input_time = 60/max_input_time = 300/' /etc/php.ini  
sed -i 's/;always_populate_raw_post_data = -1/always_populate_raw_post_data = -1/' /etc/php.ini
sed -i 's/;mbstring.func_overload = 0/mbstring.func_overload = 0/' /etc/php.ini

# 	3）创建 php-fpm 服务启动脚本：
cp sapi/fpm/init.d.php-fpm /etc/init.d/php-fpm
chmod +x /etc/init.d/php-fpm
chkconfig --add php-fpm
chkconfig php-fpm on
cp /usr/local/php5.6/etc/php-fpm.conf.default /usr/local/php5.6/etc/php-fpm.conf
sed -i 's/;pid = run\/php-fpm.pid/pid = run\/php-fpm.pid/' /usr/local/php5.6/etc/php-fpm.conf 
sed -i 's/user = nobody/user = www/' /usr/local/php5.6/etc/php-fpm.conf  
sed -i 's/group = nobody/group  = www/' /usr/local/php5.6/etc/php-fpm.conf 
sed -i 's/pm.max_children = 5/pm.max_children = 300/' /usr/local/php5.6/etc/php-fpm.conf  
sed -i 's/pm.start_servers = 2/pm.start_servers = 10/' /usr/local/php5.6/etc/php-fpm.conf  
sed -i 's/pm.min_spare_servers = 1/pm.min_spare_servers = 10/' /usr/local/php5.6/etc/php-fpm.conf
sed -i 's/pm.max_spare_servers = 3/pm.max_spare_servers = 50/' /usr/local/php5.6/etc/php-fpm.conf 
service php-fpm start

# 	4）将nginx配置文件中添加php模块
sed -i 's/#user  nobody;/user www www;/' /usr/local/nginx1.14/conf/nginx.conf 
sed -i 's/worker_processes  1;/worker_processes  4;/' /usr/local/nginx1.14/conf/nginx.conf
sed -i '/events {/a use epoll;' /usr/local/nginx1.14/conf/nginx.conf
sed -i 's/#charset koi8-r;/charset koi8-r;/' /usr/local/nginx1.14/conf/nginx.conf
sed -i 's/index  index.html index.htm;/index  index.php index.html index.htm;/' /usr/local/nginx1.14/conf/nginx.conf
sed -i '48i  location ~ \.php$ {\nroot html;\nfastcgi_pass 127.0.0.1:9000;\nfastcgi_index index.php;\ninclude fastcgi.conf;\n}' /usr/local/nginx1.14/conf/nginx.conf
sed  -i '63i  location /status {\nstub_status on;\n}' /usr/local/nginx1.14/conf/nginx.conf
nginx -s reload

# 	5）添加测试页面
echo -e '<?php\nphpinfo()\n?>' >  /usr/local/nginx1.14/html/test1.php
echo -e '<?php\n$link=mysql_connect("'"localhost"'","'"root"'","'"123"'");\nif($link) echo "ok";\nmysql_close();\n?>' > /usr/local/nginx1.14/html/test2.php 

#   6）验证页面
curl localhost -I
curl localhost/test1.php -I
curl localhost/test2.php -I

# 输出信息
echo -e "\nLNMP INSTALLED!\n"
